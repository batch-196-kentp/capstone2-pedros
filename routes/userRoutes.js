//place all user routes here
//import user controllers, import auth and destructure
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

router.post("/register",userControllers.registerUser);
router.post("/login",userControllers.loginUser);
router.get("/details",verify,userControllers.getUserDetails);
router.put("/updateAdmin/:userId",verify,verifyAdmin,userControllers.setUserAdmin);
router.get("/getAllOrders",verify,verifyAdmin,userControllers.getAllOrders);
router.get("/getOrders",verify,userControllers.getOrders);
// router.post("/checkOut",verify,userControllers.checkOut);
router.post('/checkEmailExists',userControllers.checkEmailExists);
router.get('/viewOrders', verify, userControllers.getUserOrders);
router.post('/checkOutSingleOrder', verify, userControllers.createSingleOrder);
module.exports = router; 