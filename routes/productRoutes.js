const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


router.get('/getSingleProduct/:productId',productControllers.getSingleProduct);
router.get('/active',productControllers.getActiveProducts);
router.post('/add',verify,verifyAdmin,productControllers.addProduct);
router.put('/:productId',verify,verifyAdmin,productControllers.updateProduct);
// router.delete('/archive/:productId',verify,verifyAdmin,productControllers.archiveProduct);
router.get('/', verify, verifyAdmin, productControllers.getAllProducts);
router.delete('/delete/:productId', verify, verifyAdmin, productControllers.deleteProduct)
// ARCHIVE PRODUCT (ADMIN ONLY)
router.put('/archive/:productId', verify, verifyAdmin, productControllers.archiveProduct);

// UNARCHIVE PRODUCT (ADMIN ONLY)
router.put('/unarchive/:productId', verify, verifyAdmin, productControllers.unarchiveProduct);

module.exports = router;