const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


router.get('/',verify,verifyAdmin,orderControllers.getAllOrders);
router.get('/getUserOrders',verify,orderControllers.getUserOrders);
router.post("/",verify,orderControllers.createOrder);


module.exports = router; 