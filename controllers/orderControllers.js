const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");//hashpw
const auth = require("../auth");//token

module.exports.getUserOrders = (req,res) => {
	if(req.user.isAdmin === true){
		return res.send({message:"Action Forbidden"})
	} else {
		Order.find({userId:req.user.id})
		.then(result => res.send(result))
		.catch(result => res.send(error))
	}
}
module.exports.getAllOrders = (req,res) => {
    Order.find()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.createOrder = (req,res) => {
    if(req.user.isAdmin === true) {
        return res.send({message:"Action Forbidden"})
    } else {
        let newOrder = new Order ({
            totalAmount:req.body.totalAmount,
            userId:req.user.id,
            products:req.body.products
        });
        newOrder.save()
        .then(result => res.send({message:"Order Created"}))
        .catch(error => res.send(error))
    }
};