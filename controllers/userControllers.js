//import models first
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");//hashpw
const auth = require("../auth");//token

//add controller using module.exports

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.loginUser = (req,res)=>{

	//console.log(req.body);
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null) {
			return res.send({message: "Incorrect email or password."})

		} else {
	
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send({message: "Incorrect email or password."});
			}
		}
	})

}

module.exports.getUserDetails = (req,res)=>{
	
	User.findById(req.user.id)
	.then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.setUserAdmin = (req,res) => {
    let setUser = {
        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.userId,setUser,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.getOrders = (req,res) => {
	if(req.user.isAdmin === true){
		return res.send({message:"Action Forbidden"})
	} else { 
		Order.find({userId:req.user.id})
		.then(result => res.send(result))
		.catch(result => res.send(error))
	}
}

// module.exports.createOrder = (req,res) => {
//     if(req.user.isAdmin === true) {
//         return res.send({message:"Action Forbidden"})
//     } else {
//         let newOrder = new Order ({
//             totalAmount:req.body.totalAmount,
//             userId:req.user.id,
//             products:req.body.products
//         });
//         newOrder.save()
//         .then(result => res.send({message:"Order Successful"}))
//         .catch(error => res.send(error))
//     }
// };

module.exports.getAllOrders = (req,res) => {
    Order.find()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.checkOut = async (req,res)=>{

    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"})
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        
        let newOrders = {
            totalAmount: req.body.totalAmount,
            products: req.body.products
        }

        user.orders.push(newOrders)
        return user.save()
        .then(user => true)
        .catch(err => err.message)
    })

    if(isUserUpdated !== true){
        return res.send({message:isUserUpdated})
    }

    let prodFunc = req.body.products.forEach(function(prod){
        // console.log(prod.productId)
        Products.findById(prod.productId).then(products=>{
            let buyer = {
                userId: req.user.id,
                quantity: prod.quantity
            }
            // console.log(prod.productId)
            products.orders.push(buyer);
            return products.save().then(products => true).catch(err => err.message);
        })
    })

    if(isUserUpdated){
        return res.send({message:"Thank you for buying!"})
    }
}

module.exports.createSingleOrder = async (request, response) => {
    if (request.user.isAdmin) {
        return response.send({message: "Action Forbidden"})
    }

    let isUserUpdated = await User.findById(request.user.id).then(user => {
        
        let newOrder = {
            totalAmount: request.body.totalAmount,
            products: [{
                    productId: request.body.productId,
                    quantity: request.body.quantity
                }]     
        }

        user.orders.push(newOrder);
        console.log(newOrder);

        return user.save()
        .then(user => true)
        .catch(err => err.message);
    });

    if (isUserUpdated !== true) {
        return response.send({message: isUserUpdated});
    }

    let isOrderCreated = await Product.findById(request.body.productId).then(product => {
        let checkoutOrder = {
            orderId: product.orders.id,
            userId: request.user.id,
            quantity: request.body.quantity
        }

        product.orders.push(checkoutOrder);

        return product.save()
        .then(course => true)
        .catch(err => err.message);
    });

    if (isOrderCreated !== true) {
        return response.send({message: isOrderCreated});
    }

    if (isUserUpdated && isOrderCreated) {
 
        return response.send(true);
    }
}



module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}
module.exports.getUserOrders = (request, response) => {
    User.findById(request.user.id)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}
