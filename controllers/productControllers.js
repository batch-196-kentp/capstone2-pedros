//first, import products
const Product = require("../models/Product");

module.exports.getActiveProducts = (req,res) => {
	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleProduct = (req,res) => {
	console.log(req.params.productId)//value of route params
	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.addProduct = (req,res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	console.log(newProduct);
	newProduct.save()
	.then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.updateProduct = (req,res) => {
	console.log(req.params.productId); //we got the id
	console.log(req.body); //the update inputted
	//new object to filter details
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.archiveProduct = (request, response) => {
    // console.log(request.params.productId);
    let archive = {
        isActive: false
    }

    Product.findByIdAndUpdate(request.params.productId, archive, {new: true})
    .then(archiveResult => response.send(archiveResult))
    .catch(error => result.send(error))
}


module.exports.getAllProducts = (request ,response) => {
	Product.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.deleteProduct = (request, response) => {
    Product.findByIdAndDelete(request.params.productId)
    .then(activateResult => response.send(activateResult))
    .catch(error => res.send(error))
}

module.exports.unarchiveProduct = (request, response) => {
    // console.log(request.params.productId);
    let unarchive = {
        isActive: true
    }

    Product.findByIdAndUpdate(request.params.productId, unarchive, {new: true})
    .then(activateResult => response.send(activateResult))
    .catch(error => result.send(error))
}
