//npm install on the terminal
//import, create api, assign port number then assign using listen()
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = process.env.PORT || 4000;
const cors = require('cors');
//connect to mongoose and change the name before the "?"
mongoose.connect("mongodb+srv://admin:admin123@cluster0.5ykhik8.mongodb.net/capstone2?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;
db.on('error', console.error.bind(console,"MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB."))

app.use(express.json());
app.use(cors());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);
 
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);

app.listen(port,() => console.log(`Express API running at port 4000`));