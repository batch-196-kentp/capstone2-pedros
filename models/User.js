const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({

	firstName: {
		type:String,
		required: [true,"First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin : {
		type:Boolean,
		default: false
	} 
	// orders: [

	// 	{	
	// 		products: [{
	// 			productId: {
	// 				type: String,
	// 				required: true
	// 			},
	// 			quantity: {
	// 				type: Number,
	// 				default: 1
	// 			}
	// 		}],
	// 		totalAmount: {
	// 			type: Number,
	// 			required: true	
	// 		},
	// 		purchasedOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		}

	// 	}

	// ]


})
module.exports = mongoose.model("User",userSchema); 