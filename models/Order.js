const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type:String,
		required: [true,"First name is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: true
	},
	products: [{
		productId: {
			type: String,
			required: true
		},
		quantity: {
			type: Number
		}
	}]
})
module.exports = mongoose.model("Order",orderSchema);